package org.priambodo.jsctest.model;

import java.util.ArrayList;

public class APIContact {
    public ArrayList<Result> results;
    public static class Result{
        public String gender;
        public Name name;
        public Location location;
        public String email;
        public Dob dob;
        public String phone;
        public String cell;
        public Picture picture;

        public String getName() {
            if(name.first==null)name.first = "";
            if(name.last==null)name.last = "";
            String nameS = name.first;
            if(!name.last.isEmpty())nameS+=" "+name.last;
            return nameS;
        }

        public String getShare(){
            String text = getName();
            if(email==null) email = "";
            if(!email.isEmpty()){
                text += ", "+email;
            }
            if(!phone.isEmpty()){
                text += ", "+phone;
            }
            return text;
        }

        public String getGender() {
            if(gender==null)gender = "-";
            return gender;
        }
        public String getBorn() {
            if(dob ==null || dob.date==null)dob.date = "-";
            return dob.date;
        }

        public String getEmail() {
            if(email == null)email = "";
            return email;
        }
        public String getPhone() {
            if(phone == null)phone = "";
            return phone;
        }
        public String getCellular() {
            if(cell == null)cell = "";
            return cell;
        }
        public String getAddress() {
            //location.street.name, location.street.number,
            //location.city, location.state, dan location.postcode
            if(location == null){
                return "-";
            }
            if(location.street.name==null)location.street.name = "";
            if(location.street.number==null)location.street.number = "";
            if(location.city==null)location.city = "";
            if(location.state==null)location.state = "";
            if(location.postcode==null)location.postcode = "";
            return location.street.name+"\n"+location.street.number+"\n"+location.city+"\n"+location.state+"\n"+location.postcode;
        }public String getLocation() {
            if(location == null)return "-";
            if(location.coordinates.latitude == null)location.coordinates.latitude = "-";
            if(location.coordinates.longitude == null)location.coordinates.longitude = "-";
            return location.coordinates.latitude+","+location.coordinates.longitude;
        }
        public static class Dob{
            public String date;
            public String age;
        }
        public static class Picture{
            public String large;
            public String medium;
            public String thumbnail;
        }
        public static class Name{
            public String title;
            public String first;
            public String last;
        }
        public static class Location{
            public Street street;
            public String city;
            public String state;
            public String country;
            public String postcode;
            public Coordinates coordinates;
            public Timezone timezone;
            public static class Street{
                public String number;
                public String name;
            }
            public static class Coordinates{
                public String latitude;
                public String longitude;
            }
            public static class Timezone{
                public String offset;
                public String description;
            }
        }
    }
    //{
    //  "results": [
    //    {
    //      "gender": "female",
    //      "name": {
    //        "title": "Mrs",
    //        "first": "Margie",
    //        "last": "Douglas"
    //      },
    //      "location": {
    //        "street": {
    //          "number": 1643,
    //          "name": "Photinia Ave"
    //        },
    //        "city": "Burbank",
    //        "state": "Colorado",
    //        "country": "United States",
    //        "postcode": 98463,
    //        "coordinates": {
    //          "latitude": "26.2003",
    //          "longitude": "16.8860"
    //        },
    //        "timezone": {
    //          "offset": "+3:30",
    //          "description": "Tehran"
    //        }
    //      },

    //      "email": "margie.douglas@example.com",
    //      "dob": {
    //        "date": "1977-09-12T14:46:53.226Z",
    //        "age": 45
    //      },
    //      "phone": "(548)-403-5219",
    //      "cell": "(726)-069-2100",
    //      "picture": {
    //        "large": "https://randomuser.me/api/portraits/women/44.jpg",
    //        "medium": "https://randomuser.me/api/portraits/med/women/44.jpg",
    //        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/44.jpg"
    //      }
    //    },
    //    {
    //      "gender": "female",
    //      "name": {
    //        "title": "Mrs",
    //        "first": "Marian",
    //        "last": "Johnson"
    //      },
    //      "location": {
    //        "street": {
    //          "number": 8890,
    //          "name": "Hogan St"
    //        },
    //        "city": "Bueblo",
    //        "state": "Iowa",
    //        "country": "United States",
    //        "postcode": 16763,
    //        "coordinates": {
    //          "latitude": "-42.9167",
    //          "longitude": "-141.0548"
    //        },
    //        "timezone": {
    //          "offset": "+9:30",
    //          "description": "Adelaide, Darwin"
    //        }
    //      },
    //      "email": "marian.johnson@example.com",
    //      "dob": {
    //        "date": "1957-02-21T09:15:27.437Z",
    //        "age": 65
    //      },
    //      "phone": "(548)-051-6665",
    //      "cell": "(283)-804-4707",
    //      "picture": {
    //        "large": "https://randomuser.me/api/portraits/women/29.jpg",
    //        "medium": "https://randomuser.me/api/portraits/med/women/29.jpg",
    //        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/29.jpg"
    //      }
    //    },
    //    {
    //      "gender": "male",
    //      "name": {
    //        "title": "Mr",
    //        "first": "Dustin",
    //        "last": "Woods"
    //      },
    //      "location": {
    //        "street": {
    //          "number": 655,
    //          "name": "James St"
    //        },
    //        "city": "Provo",
    //        "state": "Massachusetts",
    //        "country": "United States",
    //        "postcode": 39513,
    //        "coordinates": {
    //          "latitude": "20.5543",
    //          "longitude": "-75.2422"
    //        },
    //        "timezone": {
    //          "offset": "-10:00",
    //          "description": "Hawaii"
    //        }
    //      },
    //      "email": "dustin.woods@example.com",
    //      "dob": {
    //        "date": "1973-03-04T01:53:23.502Z",
    //        "age": 49
    //      },
    //      "phone": "(313)-198-6968",
    //      "cell": "(347)-919-6716",
    //      "picture": {
    //        "large": "https://randomuser.me/api/portraits/men/53.jpg",
    //        "medium": "https://randomuser.me/api/portraits/med/men/53.jpg",
    //        "thumbnail": "https://randomuser.me/api/portraits/thumb/men/53.jpg"
    //      }
    //    },
    //    {
    //      "gender": "female",
    //      "name": {
    //        "title": "Miss",
    //        "first": "Pamela",
    //        "last": "Wheeler"
    //      },
    //      "location": {
    //        "street": {
    //          "number": 4228,
    //          "name": "Parker Rd"
    //        },
    //        "city": "Elko",
    //        "state": "Rhode Island",
    //        "country": "United States",
    //        "postcode": 78484,
    //        "coordinates": {
    //          "latitude": "-81.7428",
    //          "longitude": "88.0021"
    //        },
    //        "timezone": {
    //          "offset": "+10:00",
    //          "description": "Eastern Australia, Guam, Vladivostok"
    //        }
    //      },
    //      "email": "pamela.wheeler@example.com",
    //      "dob": {
    //        "date": "1945-01-27T04:24:26.652Z",
    //        "age": 77
    //      },
    //      "phone": "(080)-799-6730",
    //      "cell": "(097)-494-0195",
    //      "picture": {
    //        "large": "https://randomuser.me/api/portraits/women/0.jpg",
    //        "medium": "https://randomuser.me/api/portraits/med/women/0.jpg",
    //        "thumbnail": "https://randomuser.me/api/portraits/thumb/women/0.jpg"
    //      }
    //    },
    //    {
    //      "gender": "male",
    //      "name": {
    //        "title": "Mr",
    //        "first": "Andrew",
    //        "last": "Richardson"
    //      },
    //      "location": {
    //        "street": {
    //          "number": 2062,
    //          "name": "N Stelling Rd"
    //        },
    //        "city": "San Francisco",
    //        "state": "Maine",
    //        "country": "United States",
    //        "postcode": 54405,
    //        "coordinates": {
    //          "latitude": "4.8987",
    //          "longitude": "-101.2161"
    //        },
    //        "timezone": {
    //          "offset": "+2:00",
    //          "description": "Kaliningrad, South Africa"
    //        }
    //      },
    //      "email": "andrew.richardson@example.com",
    //      "dob": {
    //        "date": "1972-01-05T01:36:34.591Z",
    //        "age": 50
    //      },
    //      "phone": "(219)-705-9314",
    //      "cell": "(410)-674-8353",
    //      "picture": {
    //        "large": "https://randomuser.me/api/portraits/men/27.jpg",
    //        "medium": "https://randomuser.me/api/portraits/med/men/27.jpg",
    //        "thumbnail": "https://randomuser.me/api/portraits/thumb/men/27.jpg"
    //      }
    //    }
    //  ]
    //}
}
