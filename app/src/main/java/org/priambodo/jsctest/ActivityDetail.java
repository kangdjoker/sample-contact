package org.priambodo.jsctest;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.priambodo.jsctest.model.APIContact;

public class ActivityDetail extends AppCompatActivity implements View.OnClickListener {

    APIContact.Result result;
    ImageView imageView;
    TextView txtName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle("Rincian Kontak");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        result = new Gson().fromJson(getIntent().getStringExtra("data"),APIContact.Result.class);
        imageView = findViewById(R.id.img);
        Glide.with(this).load(""+result.picture.large).into(imageView);
        txtName = findViewById(R.id.txt_name);
        txtName.setText(""+result.getName());
        TextView txtBorn = findViewById(R.id.txt_born);
        TextView txtGender = findViewById(R.id.txt_gender);
        txtBorn.setText(result.getBorn());
        txtGender.setText(result.getGender());
        TextView txtEmail = findViewById(R.id.txt_email);
        txtEmail.setOnClickListener(this);
        TextView txtPhone = findViewById(R.id.txt_phone);
        TextView txtCellular = findViewById(R.id.txt_cellular);
        TextView txtLocation = findViewById(R.id.txt_location);
        txtLocation.setOnClickListener(this);
        TextView txtAddress = findViewById(R.id.txt_address);
        txtEmail.setText(result.getEmail());
        txtPhone.setText(result.getPhone());
        txtCellular.setText(result.getCellular());
        txtLocation.setText(result.getLocation());
        txtAddress.setText(result.getAddress());
        findViewById(R.id.btn_copy).setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, result.getShare());
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_copy:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", ""+result.getName());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(this,"Nama telah tersalin",Toast.LENGTH_LONG).show();
                break;
            case R.id.txt_email:
//                Intent intent = new Intent(Intent.ACTION_SENDTO);
//                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
//                intent.putExtra(Intent.EXTRA_EMAIL, result.getEmail());
//                intent.putExtra(Intent.EXTRA_SUBJECT, "");
//                startActivity(intent);
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{result.getEmail()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Hi");
                intent.putExtra(Intent.EXTRA_TEXT,"Hi");
                startActivity(intent);
                break;
            case R.id.txt_location:
                String geoUri = "http://maps.google.com/maps?q=loc:" + result.location.coordinates.latitude + "," + result.location.coordinates.longitude + " (" + result.getName() + ")";
                Intent intentmap = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intentmap);

                break;
        }

    }
}
