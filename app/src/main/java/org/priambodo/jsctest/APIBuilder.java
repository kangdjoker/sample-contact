package org.priambodo.jsctest;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class APIBuilder {
    static Request buildGET(String finalURL,Response.Listener<String> success,Response.ErrorListener failed){
        return new StringRequest(Request.Method.GET, finalURL,success, failed);
    }
}
