package org.priambodo.jsctest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.priambodo.jsctest.model.APIContact;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RequestQueue requestQueue;
    private static final String TAG="TAG";
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView list;
    AdapterContact adapterContact;
    boolean isAPI;

    @Override
    protected void onResume() {
        super.onResume();
        if(!isAPI){
            isAPI = true;
            updateValue();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeRefreshLayout = findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(this);
        list = findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(linearLayoutManager);
        adapterContact = new AdapterContact();
        getSupportActionBar().setTitle("Pilih Kontak");
        list.setAdapter(adapterContact);
    }

    class ViewHolderContact extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;
        ImageView img;
        View container;

        public ViewHolderContact(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            img = itemView.findViewById(R.id.img);
            container = itemView.findViewById(R.id.container);
            container.setOnClickListener(this);
        }

        public void updateAsset(APIContact.Result result) {
            container.setTag(result);
            name.setText(result.getName());
            Glide.with(itemView.getContext())
                    .load(result.picture.thumbnail)
                    .transform(new RoundedCorners(Tools.dip2px(itemView.getContext(),200)))
                    .into(img);
        }

        @Override
        public void onClick(View view) {
            APIContact.Result result = (APIContact.Result)view.getTag();
            Intent intent = new Intent(MainActivity.this,ActivityDetail.class);
            intent.putExtra("data",new Gson().toJson(result));
            startActivity(intent);
        }
    }

    class AdapterContact extends RecyclerView.Adapter<ViewHolderContact>{
        ArrayList<APIContact.Result> results;
        public AdapterContact(){
            this.results = new ArrayList<>();
        }
        @NonNull
        @Override
        public ViewHolderContact onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
            return new ViewHolderContact(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderContact holder, int position) {
            holder.updateAsset(results.get(position));
        }

        @Override
        public int getItemCount() {
            return results.size();
        }

        public void updateAsset(ArrayList<APIContact.Result> results) {
            this.results = results;
            notifyDataSetChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestQueue = Volley.newRequestQueue(this);;
    }

    void addRequest(Request request){
        request.setTag(TAG);
        requestQueue.add(request);
    }

    @Override
    protected void onStop() {
        requestQueue.cancelAll(TAG);
        super.onStop();
    }

    void updateValue(){
        addRequest(APIBuilder.buildGET("https://randomuser.me/api?results=5&exc=login,registered,id,nat&nat=us&noinfo", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    APIContact apiContact = new Gson().fromJson(response,APIContact.class);
                    adapterContact.updateAsset(apiContact.results);
                }catch (Exception e){

                }
                swipeRefreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
            }
        }));
    }

    @Override
    public void onRefresh() {
        updateValue();
    }
}